package us.genly.elephants.awt;

import java.awt.Color;
import java.awt.Graphics;

import us.genly.elephants.graphics.EColor;
import us.genly.elephants.graphics.EGraphics;

public class AwtEGraphics implements EGraphics {
    Graphics g;
    private float _scale  = 1;

    int scale(float f) {
        return (int) (f * _scale);
    }

    public void setScale(float s) {
        _scale = s;
    }

    public AwtEGraphics(Graphics g) {
        this.g = g;
    }

    public void drawOval(float x, float y, float w, float h) {
        g.drawOval(scale(x), scale(y), scale(w), scale(h));
    }

    public void fillOval(float x, float y, float w, float h) {
        g.fillOval(scale(x), scale(y), scale(w), scale(h));
    }

    public void fillRect(float x, float y, float w, float h) {
        g.fillRect(scale(x), scale(y), scale(w), scale(h));
    }

    public void drawLine(float x0, float y0, float x1, float y1) {
        g.drawLine(scale(x0), scale(y0), scale(x1), scale(y1));
    }

    public void setColor(EColor color) {
        g.setColor(new Color(color.r, color.g, color.b));
    }
}
