package us.genly.elephants.awt;

import java.awt.*;

/**
 * A screen saver fills the screen, adds peanuts occasionally. Closes
 * when there is mouse or keyboard activity.
 */
public class ElephantsScreenSaver extends AbstractElephantApp {

    @Override
    protected Window createTopLevelWindow() {
        return new ScreenSaverWindow();
    }

    @Override
    protected boolean getShowThoughts() {
        return true;
    }

    @Override
    protected int getElephantCount() {
        return 5;
    }

    @Override
    protected int getPeanutCount() {
        return 5;
    }

    public static void main(String args[]) {
        new ElephantsScreenSaver();
    }
}
