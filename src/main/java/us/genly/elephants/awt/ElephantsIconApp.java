package us.genly.elephants.awt;

import us.genly.elephants.actors.Elephant;

import java.awt.*;


public class ElephantsIconApp extends AbstractElephantApp {

    @Override
    protected Window createTopLevelWindow() {
        return new Frame("Elephant");
    }

    @Override
    protected boolean getShowThoughts() {
        return false;
    }

    @Override
    protected boolean getAnimate() {
        return false;
    }

    @Override
    protected void createElephants() {
        Elephant elephant = new Elephant(_elephantCanvas._room);
        float scale = 6;
        elephant.setX((int) (WIDTH /2/scale));
        elephant.setY((int) (HEIGHT /2/scale));
        _elephantCanvas.setScale(scale);
        _elephantCanvas._room.add(elephant);
    }

    public void createPeanutsOccasionally() {

    }

    @Override
    protected int getElephantCount() {
        return 1;
    }

    @Override
    protected int getPeanutCount() {
        return 0;
    }

    public static void main(String args[]) {
        new ElephantsIconApp();
    }
}
