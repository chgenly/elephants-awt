package us.genly.elephants.awt;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import us.genly.elephants.actors.Mouse;
import us.genly.elephants.actors.Peanut;
import us.genly.elephants.actors.Room;
import us.genly.elephants.graphics.EGraphics;

public class ElephantsCanvas extends Canvas implements Runnable {
    private static final long serialVersionUID = 4456207048643121947L;
    int w, h;
    Thread thread;
    Image osi;
    Graphics osg;
    EGraphics eosg;
    Image outside;
    public Room _room;
    private static final java.util.Random RANDOM = new java.util.Random(System.currentTimeMillis());
    private boolean _animate = true;
    private float _scale = 1;

    public ElephantsCanvas() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getID() == MouseEvent.MOUSE_CLICKED) {
                    if ((e.getModifiers()  & MouseEvent.BUTTON1_MASK) != 0)
                        feedPeanut(e.getX(), e.getY());
                    else
                        createMouse(e.getX(), e.getY());
                }
            }
        });
    }

    public void init(Image outside) {
        this.outside = outside;
    }

    public void setRoom(Room room) {
        this._room = room;
    }

    public void setAnimate(boolean animate) {
        _animate = animate;
    }

    public void setScale(float scale) {
        _scale = scale;
    }

    public void start() {
        w = getSize().width;
        h = getSize().height;
        osi = createImage(w, h);
        osg = osi.getGraphics();
        eosg = new AwtEGraphics(osg);
        eosg.setScale(_scale);

        _room.setWidth(w);
        _room.setHeight(h);

        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        thread = null;
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        if (g == null)
            return;

        g.drawImage(osi, 0, 0, null);
    }

    public void run() {
        try {
            while (thread != null) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        osg.setColor(Color.black);
                        osg.fillRect(0, 0, w, h);
                        osg.drawImage(outside, 0, 0, null);
                        _room.next();
                        _room.paint(eosg);

                        repaint();
                    }
                });
                if (!_animate)
                    break;
                Thread.sleep(100);
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
            System.exit(10);
        }

    }

    public void feedPeanut(int x, int y) {
        _room.add(new Peanut(x, y));
    }

    public void createMouse(int x, int y) {
        _room.add(new Mouse(_room, x, y));
    }
}
