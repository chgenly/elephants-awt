package us.genly.elephants.awt;

import us.genly.elephants.actors.Actor;
import us.genly.elephants.actors.Elephant;
import us.genly.elephants.actors.Peanut;
import us.genly.elephants.actors.Room;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


/**
 * Base class for the ElephantsApp and ElephantsScreenSaver
 */
public abstract class AbstractElephantApp implements Runnable {
    protected final int WIDTH = 1024;
    protected final int HEIGHT = 768;
    protected ElephantsCanvas _elephantCanvas;

    AbstractElephantApp() {
        Window f = createTopLevelWindow();
        f.addWindowListener(closeOnExit());

        Image outside = null;

        try {
            outside = Toolkit.getDefaultToolkit().getImage(new java.net.URL("file:bedrooms.gif"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        _elephantCanvas = new ElephantsCanvas();
        _elephantCanvas.setSize(WIDTH, HEIGHT);
        _elephantCanvas.setAnimate(getAnimate());
        _elephantCanvas.setScale(getScale());
        int w = _elephantCanvas.getWidth();
        int h = _elephantCanvas.getHeight();
        Room room = new Room(w, h);
        room.setScale(3);
        _elephantCanvas.setRoom(room);
        f.add(_elephantCanvas);
        f.pack();
        f.setVisible(true);

        _elephantCanvas.init(outside);

        Elephant.setShowThoughts(getShowThoughts());

        createPeanuts();

        createElephants();

        _elephantCanvas.start();

        createPeanutsOccasionally();
    }

    private float getScale() {
        return 1;
    }

    protected boolean getAnimate() {
        return true;
    }

    public void createPeanutsOccasionally() {
        new Thread(this).start();
    }

    protected void createElephants() {
        for (int i = 0; i < getElephantCount(); ++i) {
            Elephant elephant = new Elephant(_elephantCanvas._room);
             _elephantCanvas._room.add(elephant);
        }
    }

    protected void createPeanuts() {
        for (int i = 0; i < getPeanutCount(); ++i) {
            int x = (int) (_elephantCanvas.getSize().width * Math.random());
            int y = (int) (_elephantCanvas.getSize().height * Math.random());
            _elephantCanvas._room.add(new Peanut(x, y));
        }
    }

    protected abstract boolean getShowThoughts();

    protected abstract int getPeanutCount();

    protected abstract int getElephantCount();

    protected abstract Window createTopLevelWindow();

    private static WindowListener closeOnExit() {
        return new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        };
    }

    public void run() {
        try {
            for (; ; ) {
                // Every fourty seconds make sure there are peanuts for the elephants
                Thread.sleep(40000);
                EventQueue.invokeLater(new Runnable() {
                    public void run() {

                        boolean found = false;
                        for (Actor o : _elephantCanvas._room.roomObjects()) {
                            if (o instanceof Peanut) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            for (int i = 0; i < getPeanutCount(); ++i) {
                                int x = (int) (_elephantCanvas.getSize().width * Math.random());
                                int y = (int) (_elephantCanvas.getSize().height * Math.random());
                                _elephantCanvas._room.add(new Peanut(x, y));
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(10);
        }
    }
}
