package us.genly.elephants.awt;
import java.awt.*;

class ScreenSaverWindow extends Window {
	private static final long serialVersionUID = 1362777766927769167L;
	boolean first = true;
    int x;
    int y;

    public ScreenSaverWindow()
    {
        super(new Frame());
    }

    public boolean handleEvent(Event evt)
    {
        switch(evt.id) {
        case Event.MOUSE_MOVE:
            System.out.println("mouse move at ("+evt.x+", "+evt.y+")");
            if (first) {
                // At startup we get a mouse move event, don't let it stop the
                // screen saver.
                this.x = evt.x;
                this.y = evt.y;
                first = false;
                return true;
            } else {
                // If we get a mouse move event and the mouse hasn't moved,
                // ignore it.  Windows screen saver code seems to send this.
                if (evt.x == this.x && evt.y == this.y) {
                    System.out.println("Ignoring mouse move");
                    return true;
                }
            }

        case Event.MOUSE_DOWN:
        case Event.KEY_PRESS:
            System.exit(0);
            break;
        }
        return true;
    }
}