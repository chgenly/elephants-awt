package us.genly.elephants.awt;

import java.awt.*;


public class ElephantsApp extends AbstractElephantApp {

    @Override
    protected Window createTopLevelWindow() {
        return new Frame("Elephant");
    }

    @Override
    protected boolean getShowThoughts() {
        return true;
    }

    @Override
    protected int getElephantCount() {
        return 5;
    }

    @Override
    protected int getPeanutCount() {
        return 5;
    }

   public static void main(String args[]) {
        new ElephantsApp();
    }
}
