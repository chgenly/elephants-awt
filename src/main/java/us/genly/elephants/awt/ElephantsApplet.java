package us.genly.elephants.awt;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;
import java.net.URL;
import java.util.Random;

import us.genly.elephants.actors.Elephant;
import us.genly.elephants.actors.Mouse;
import us.genly.elephants.actors.Peanut;
import us.genly.elephants.actors.Room;


public class ElephantsApplet extends Applet {
	private static final long serialVersionUID = 2054541969365831344L;
	int w, h;
    Image outside;
    
    Random rand = new Random(System.currentTimeMillis());

	java.awt.Panel buttonPanel;
	java.awt.Button feedButton;
	java.awt.Button mouseButton;
	java.awt.Checkbox showThoughts;
    
    ElephantsCanvas canvas;

    public void init() {
        try {
        	URL url = getClass().getClassLoader().getResource("weeds.gif");
        	outside = getImage(url);
        } catch(Exception e) {
            e.printStackTrace();
        }
        
		setLayout(new BorderLayout(0,0));
		setSize(506,383);
		buttonPanel = new java.awt.Panel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER,5,5));
		buttonPanel.setBounds(0,0,506,10);
		buttonPanel.setBackground(java.awt.Color.white);
		add("North", buttonPanel);
		feedButton = new java.awt.Button();
		feedButton.setLabel("Feed");
		feedButton.setBounds(145,5,42,23);
		feedButton.setBackground(java.awt.Color.lightGray);
		buttonPanel.add(feedButton);
		mouseButton = new java.awt.Button();
		mouseButton.setActionCommand("button");
		mouseButton.setLabel("Mouse");
		mouseButton.setBounds(192,5,51,23);
		mouseButton.setBackground(java.awt.Color.lightGray);
		buttonPanel.add(mouseButton);
		showThoughts = new java.awt.Checkbox("Show thoughts");
		showThoughts.setBounds(248,5,112,23);
		buttonPanel.add(showThoughts);

		canvas = new ElephantsCanvas();
        canvas.init(outside);
        add("Center", canvas);
        
        doLayout();
        
        w = canvas.getSize().width;
        h = canvas.getSize().height;
        Room room = new Room(w, h);
        canvas.setRoom(room);
        

        int ecnt=3;
        String param = getParameter("elephants");
        if (param != null) {
            try {
                ecnt = Integer.parseInt(param);
            } catch(Exception e) {
            }
        }
        for(int i=0; i<ecnt; ++i) {
            Elephant e = new Elephant(canvas._room);
            e.setShock(i%2 == 0);
            canvas._room.add(e);
        }
    }
    
    public void start()
    {
        canvas.start();
    }

    public void stop()
    {
        canvas.stop();
    }

    public boolean action(Event evt, Object what)
    {
        if (evt.target == feedButton) {
            for(int i=0; i<5; ++i) {
                int x = (int)(w*java.lang.Math.random());
                int y = (int)(h*java.lang.Math.random());
                canvas._room.add(new Peanut(x, y));
            }
            return true;
        } else
        if (evt.target == showThoughts) {
            Elephant.setShowThoughts(showThoughts.getState());
        } else
        if (evt.target == mouseButton) {
            int x = (int)(w*java.lang.Math.random());
            int y = (int)(h*java.lang.Math.random());
            canvas._room.add(new Mouse(canvas._room, x, y));
        }
        return false;
    }

    public static void main(String args[])
    {
        Window f = new ScreenSaverWindow();
        f.toFront();   

        Image outside=null;
        try {
            outside = Toolkit.getDefaultToolkit().getImage(new java.net.URL("file:bedroom.gif"));
        } catch(Exception e) {
            e.printStackTrace();
        }

        ElephantsCanvas ec = new ElephantsCanvas();
        ec.setSize(1024,742);
        f.add(ec);
        f.pack();
        f.setVisible(true);

        ec.init(outside);
        Elephant.setShowThoughts(true);
            for(int i=0; i<5; ++i) {
                int x = (int)(ec.getSize().width*java.lang.Math.random());
                int y = (int)(ec.getSize().height*java.lang.Math.random());
                ec._room.add(new Peanut(x, y));
            }

        for(int i=0; i<5; ++i) {
            Elephant e = new Elephant(ec._room);
            e.setShock(i%2 == 0);
            ec._room.add(e);
        }
        ec.start();
    }
}
