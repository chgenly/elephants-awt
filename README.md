# README #

# Mango RoseMilk's Elephants #

This little group of elephants likes to herd together, holding on to each other's tails. If you give them some peanuts, 
they will compete over the food, so be nice and give them enough for everyone.  Be careful with the mice, we don't want
them to panic and cause a stampede, now do we? If you want to know what the elephants are thinking click on Show thoughts.
A colored line will appear for each elephant showing you what it wants to go towards, or away from.

* green - is for following another elephant 
* yellow - is for going after a peanut 
* red - is for running away from the mouse. 
* You can drop a peanut by clicking on their room.

This adorable little herd of Java elephants was created by me and my Dad in our spare time. 
It is by no means completed, as we work on it every chance we get, And we will try to keep 
it updated as much as possible.

### How do I get set up? ###

* Get the elephant library  
    // https://bitbucket.org/chgenly/elephants-lib  
	git clone git@bitbucket.org:chgenly/elephants-lib.git  
	cd elephants-lib  
	gradle publishToMavenLocal  
* Get this project and build it using  
    gradle jar  
	java -jar build/libs/elephants-awt-1.0-SNAPSHOT.jar  
